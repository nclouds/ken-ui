
$(document).ready(function(){

	$('.js-btn-feeds').on('click', function(){
		$('body').toggleClass('feeds-on');
	});

	$('.js-btn-search-expand ').on('click', function(){
		$(this).parent().toggleClass('active');
		$(this).parent().find('input').val('');
	});

	$('.js-btn-search-expand').parent().find('input').blur(function(){
		$(this).parent().removeClass('active');
	});

	$('.js-btn-open-breadcum').on('click', function(){
		$(this).parent().toggleClass('open');
		return false;
	});

});